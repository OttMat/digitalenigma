package application;

import java.io.FileNotFoundException;

import javafx.application.Application;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import mechanics.Enigma;
import mechanics.Plugboard;
import mechanics.Rotor;

public class Main extends Application {

	private MenuBar createMenuBar(ReadOnlyDoubleProperty menuWidthProperty) {
		MenuBar menuBar = new MenuBar();
		Menu menuFile = new Menu("File");
		Menu menuAbout = new Menu("About");

		MenuItem newEnigma = new MenuItem("New Enigma");
		newEnigma.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				createNewEnigma();
			}
		});
		MenuItem options = new MenuItem("Options");
		options.setDisable(true);
		options.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				openEditWindow();
			}
		});
		MenuItem exit = new MenuItem("Exit");
		exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				System.exit(0);
			}
		});
		menuFile.getItems().addAll(newEnigma, options, new SeparatorMenuItem(),
				exit);

		MenuItem info = new MenuItem("Info");
		info.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				openInfoWindow();
			}
		});
		menuAbout.getItems().add(info);

		menuBar.getMenus().addAll(menuFile, menuAbout);
		menuBar.prefWidthProperty().bind(menuWidthProperty);

		return menuBar;
	}

	private void openInfoWindow() {
		Stage stage = new Stage();
		GridPane root = new GridPane();
		root.setPadding(new Insets(10, 15, 15, 10));
		setStageSize(stage, 100, 180);
		Label labelNimi = new Label("Ott Matiisen");
		Label labelKontakt = new Label("ottmatiisen@hotmail.com");
		root.add(labelNimi, 0, 6);
		root.add(labelKontakt, 0, 7);

		Scene scene = new Scene(root, 40, 100);
		stage.setScene(scene);
		stage.show();

	}

	private void setStageSize(Stage stage, double height, double width) {
		stage.setMaxHeight(height);
		stage.setMaxWidth(width);
		stage.setMinHeight(height);
		stage.setMinWidth(width);
	}

	private void openEditWindow() {
		Stage stage = new Stage();
		Pane root = new Pane();
		Scene scene = new Scene(root, 40, 100);
		stage.setScene(scene);
		stage.show();

	}

	private void createNewEnigma() {
		Enigma enigma = new Enigma(new Rotor("JGDQOXUSCAMIFRVTPNEWKBLZYH"),
				new Rotor("NTZPSFBOKMWRCJDIVLAEYUXHGQ"), new Rotor(
						"JVIUBHTCDYAKEQZPOSGXNRMWFL"), new Rotor(
						"QYHOGNECVPUZTFDJAXWMKISRBL"), new Plugboard(true));

		Stage stage = new Stage();
		Group root = new Group();
		Scene scene = new Scene(root);
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 15, 10, 15));
		root.getChildren().add(grid);

		grid.getColumnConstraints().add(new ColumnConstraints());
		grid.getRowConstraints().add(new RowConstraints());
		grid.add(new Label("S�num:"), 0, 0);
		grid.add(new Label("�ifreerituna:"), 0, 3);
		TextField message = new TextField("Kirjuta siia");
		grid.add(message, 3, 0);
		grid.getRowConstraints().add(new RowConstraints(20));
		grid.getColumnConstraints().add(new ColumnConstraints(20));
		Label messageLabel = new Label();
		grid.add(messageLabel, 3, 3);
		Button cipher = new Button("Cipher");
		cipher.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				String cipheredMessage = enigma.cipher(message.getText());
				messageLabel.setText(cipheredMessage);
			}
		});
		grid.add(cipher, 5, 0);
		Button backButton = new Button("Back");
		backButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		grid.add(backButton, 5, 11);
		stage.setScene(scene);
		stage.show();
	}

	private Scene createStartMenu(Group root) throws FileNotFoundException {
		Scene scene = new Scene(root);
		// ImageView splashScreen = new ImageView(new Image(new
		// FileInputStream("img//Enigma.jpg")));
		// root.getChildren().addAll(splashScreen);
		GridPane pane = new GridPane();
		pane.setPadding(new Insets(100, 40, 40, 90));
		Button start = new Button("Start");
		start.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				createNewEnigma();
			}
		});
		pane.getColumnConstraints().add(new ColumnConstraints(3));
		pane.add(start, 0, 0, 2, 1);
		Button exit = new Button("Exit");
		exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				System.exit(0);
			}
		});
		pane.getRowConstraints().add(new RowConstraints(60));
		pane.add(exit, 1, 2);
		root.getChildren().add(pane);
		return scene;
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			setStageSize(primaryStage, 350, 250);
			Group rootGroup = new Group();
			MenuBar menuBar = createMenuBar(primaryStage.widthProperty());
			Scene scene = createStartMenu(rootGroup);
			rootGroup.getChildren().addAll(menuBar);
			scene.getStylesheets().add(
					getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("Digital Enigma");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}