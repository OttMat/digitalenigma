package mechanics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Plugboard {

	Map<Character, Character> pairs = new HashMap<Character, Character>();

	public Plugboard(boolean fill) {
		super();
		if (true) {
			ArrayList<Integer> kasutatud = new ArrayList<Integer>();
			int i = 0;
			while (i < 20) {
				int katse = (int) (Math.round((Math.random() * 25) + 65));
				if (!kasutatud.contains(katse)) {
					kasutatud.add(katse);
					i++;
					while (i % 2 == 1) {
						int vaste = (int) (Math.round((Math.random() * 25) + 65));
						if (!kasutatud.contains(vaste)) {
							kasutatud.add(vaste);
							i++;
							this.addPair((char) katse, (char) vaste);
						}
					}
				}
			}
			this.addOthers();
		}
	}

	public char translate(char ch) {
		return pairs.get(ch);
	}

	public void addPair(char one, char two) {
		if (pairs.size() < 26) {
			pairs.put(one, two);
			pairs.put(two, one);
		} else {
			throw new RuntimeException("Liiga palju \"juhtmepaare\"");
		}
	}

	public void addOthers() {
		for (char i = 'A'; i <= 'Z'; i++) {
			pairs.putIfAbsent(i, i);
		}
	}
}
