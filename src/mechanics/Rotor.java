package mechanics;

/**
 * @author Ott
 *
 */
public class Rotor {

	private String cipher;
	private int position = 0;

	/**
	 * Konstruktor rootori loomiseks, positsiooni pole vaja eraldi defineerida
	 * 
	 * @param cipher
	 */
	public Rotor(String cipher) {
		super();
		this.cipher = cipher;
	}

	/**
	 * Tagastab rootori asendi
	 * @return
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Muudab rootori asendit vastavalt etteantud positsioonile
	 * @param position
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	
	/**
	 * V�tab t�hestiku t�he ning tagastab �ifrist vastavast asendist t�he.
	 * Kuna java jaoks on "char" ehk t�ht �htlasi t�isarv, saab ASCII t�hest
	 * 65 lahutades t�he indeksi t�hestikus ("A" on ASCII's 65 ning t�hestikus
	 * indeksiga 0). Kuna minu masinas liigub rootori �iffer paremale, 
	 *  
	 * @param letter
	 * @return
	 */
	public char into(char letter) {
		if ((letter - 65 - position) >= 0) {
			return cipher.charAt(letter - 65 - position);
		} else {
			return cipher.charAt(letter - 39 - position);
		}
	}

	public char out(char letter) {
		if ((cipher.indexOf(letter) + position + 65) < 91) {
			return (char) (cipher.indexOf(letter) + position + 65);
		} else {
			return (char) (cipher.indexOf(letter) + position + 39);
		}
	}
}