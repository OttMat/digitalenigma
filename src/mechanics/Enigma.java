package mechanics;

public class Enigma {

	private Rotor rotor1;
	private Rotor rotor2;
	private Rotor rotor3;
	private Rotor mirror;
	private Plugboard plugboard;

	public Enigma(Rotor rotor1, Rotor rotor2, Rotor rotor3, Rotor mirror,
			Plugboard plugboard) {
		super();
		this.rotor1 = rotor1;
		this.rotor2 = rotor2;
		this.rotor3 = rotor3;
		this.mirror = mirror;
		this.plugboard = plugboard;
	}

	public int[] getCalibration() {
		return new int[] { rotor1.getPosition(), rotor2.getPosition(),
				rotor3.getPosition() };
	}

	public void turn() {
		if (rotor3.getPosition() < 25) {
			rotor3.setPosition(rotor3.getPosition() + 1);
		} else {
			rotor3.setPosition(0);
			if (rotor2.getPosition() < 25) {
				rotor2.setPosition(rotor2.getPosition() + 1);
			} else {
				rotor2.setPosition(0);
				if (rotor1.getPosition() < 25) {
					rotor1.setPosition(rotor1.getPosition() + 1);
				} else {
					rotor1.setPosition(0);
				}
			}
		}
	}

	public void calibrate(int firstRotorPosition, int secondRotorPosition,
			int thirdRotorPosition) {
		rotor1.setPosition(firstRotorPosition);
		rotor2.setPosition(secondRotorPosition);
		rotor3.setPosition(thirdRotorPosition);
	}

	public String cipher(String message) {
		message = message.toUpperCase();
		message = message.replaceAll("�", "AE");
		message = message.replaceAll("�", "Y");
		message = message.replaceAll("�", "OE");
		message = message.replaceAll("�", "O");
		for (int i = 0; i < message.length(); i++) {
			if ((message.charAt(i) < 'A') || (message
					.charAt(i) > 'Z')) {
				message = message.replaceAll(
						Character.toString(message.charAt(i)), " ");
			}
		}
		String[] strippedText = message.split("");
		String properMessage = "";
		for (String bit : strippedText) {
			properMessage += bit.trim();
		}
		String cipheredMessage = "";
		for (int i = 0; i < properMessage.length(); i++) {
			if (properMessage.charAt(i) > 64 && properMessage.charAt(i) < 91) {
				this.turn();
				char letter = plugboard.translate(rotor1.out(rotor2.out(rotor3.out(mirror
								.into(rotor3.into(rotor2.into(rotor1
										.into(plugboard.translate(
												properMessage.charAt(i))))))))));
				cipheredMessage += letter;
			} else {
				throw new RuntimeException(
						"S�mbol ei kuulu ladina t�hestikku: "
								+ message.charAt(i));
			}
		}
		return cipheredMessage;
	}
}